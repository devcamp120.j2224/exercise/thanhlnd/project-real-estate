package com.devcamp.project.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.project.realestate.model.CAddressMap;
import org.springframework.stereotype.Repository;

@Repository
public interface IAddressMapRepository extends JpaRepository<CAddressMap, Integer>  {
    
}
