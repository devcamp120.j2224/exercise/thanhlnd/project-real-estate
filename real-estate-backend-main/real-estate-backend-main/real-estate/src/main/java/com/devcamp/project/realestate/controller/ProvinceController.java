package com.devcamp.project.realestate.controller;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.*;
import com.devcamp.project.realestate.service.ProvinceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class ProvinceController {
    @Autowired
    IProvinceRepository pProvinceRepository;

    @Autowired
    ProvinceService pProvinceService;

    @GetMapping("/provinces")
    public ResponseEntity<Object> getProvincesList() {
        if (!pProvinceService.getProvinceList().isEmpty()) {
            return new ResponseEntity<>(pProvinceService.getProvinceList(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/provinces/{provinceId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getProvinceById(@PathVariable Integer provinceId) {
        if (pProvinceRepository.findById(provinceId).isPresent()) {
            return new ResponseEntity<>(pProvinceRepository.findById(provinceId).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/provinces")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> createProvince(@RequestBody CProvince cProvince) {
        try {
            return new ResponseEntity<>(pProvinceService.createProvinceObj(cProvince), HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Province: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/provinces/{provinceId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> updateProvince(@PathVariable("provinceId") Integer provinceId, @RequestBody CProvince cProvince) {
        try {
            Optional<CProvince> provinceData = pProvinceRepository.findById(provinceId);
            if (provinceData.isPresent()) {
                return new ResponseEntity<>(pProvinceService.updateProvinceObj(cProvince, provinceData), HttpStatus.OK);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Province: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>("Province NOT FOUND!", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/provinces/{provinceId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> deleteProvinceById(@PathVariable Integer provinceId) {
        try {
            pProvinceRepository.deleteById(provinceId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
