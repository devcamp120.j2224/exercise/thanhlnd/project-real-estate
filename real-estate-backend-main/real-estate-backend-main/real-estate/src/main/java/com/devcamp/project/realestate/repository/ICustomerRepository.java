package com.devcamp.project.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.project.realestate.model.CCustomer;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ICustomerRepository extends JpaRepository<CCustomer, Integer> {
    
    @Modifying
    @Transactional
    @Query(value = "DELETE FROM `customers` WHERE `id` = :customerId ;", nativeQuery = true)
    Object deleteCustomerById(@Param("customerId") int customerId);
}
