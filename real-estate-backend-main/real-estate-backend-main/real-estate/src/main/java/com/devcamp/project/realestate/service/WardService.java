package com.devcamp.project.realestate.service;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.IWardRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WardService {
    @Autowired
    IWardRepository pWardRepository;

    public List<CWard> getWardList() {
        List<CWard> wardList = new ArrayList<CWard>();
        pWardRepository.findAll().forEach(wardList::add);
        return wardList;
    }

    public Object createWardObj(CWard cWard, Optional<CDistrict> districtData) {

        CWard newWard = new CWard();
        newWard.setName(cWard.getName());
        newWard.setPrefix(cWard.getPrefix());

        CDistrict _district = districtData.get();
        newWard.set_district(_district);

        CWard savedWard = pWardRepository.save(newWard);
        return savedWard;
    }

    public Object updateWardObj(CWard cWard, Optional<CWard> wardData) {

        CWard newWard = wardData.get();
        newWard.setName(cWard.getName());
        newWard.setPrefix(cWard.getPrefix());

        CWard savedWard = pWardRepository.save(newWard);
        return savedWard;
    }

}
