package com.devcamp.project.realestate.controller;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.*;
import com.devcamp.project.realestate.service.CustomerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CustomerController {
    @Autowired
    ICustomerRepository pCustomerRepository;

    @Autowired
    CustomerService pCustomerService;

    @GetMapping("/customers")
    @PreAuthorize("hasAnyRole('ADMIN', 'DEFAULT', 'HOMESELLER')")
    public ResponseEntity<Object> getCustomerList() {
        if (!pCustomerService.getCustomerList().isEmpty()) {
            return new ResponseEntity<>(pCustomerService.getCustomerList(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/customers/{customerId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getCustomerById(@PathVariable Integer customerId) {
        if (pCustomerRepository.findById(customerId).isPresent()) {
            return new ResponseEntity<>(pCustomerRepository.findById(customerId).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/customers")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> createCustomer(@RequestBody CCustomer cCustomer) {
        try {
            return new ResponseEntity<>(pCustomerService.createCustomerObj(cCustomer), HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Customer: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/customers/{customerId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> updateCustomer(@PathVariable("customerId") Integer customerId, @RequestBody CCustomer cCustomer) {
        try {
            Optional<CCustomer> customerData = pCustomerRepository.findById(customerId);
            if (customerData.isPresent()) {
                return new ResponseEntity<>(pCustomerService.updateCustomerObj(cCustomer, customerData), HttpStatus.OK);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Customer: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>("Customer NOT FOUND!", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/customers/{customerId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable int customerId) {
        try {
            pCustomerRepository.deleteCustomerById(customerId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
