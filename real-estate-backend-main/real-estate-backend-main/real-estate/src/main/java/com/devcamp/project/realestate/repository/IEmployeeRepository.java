package com.devcamp.project.realestate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.devcamp.project.realestate.model.CEmployees;

@Repository
public interface IEmployeeRepository extends JpaRepository<CEmployees, Integer> {
    
    CEmployees findByUsername(String username);

    @Query(value = " SELECT * FROM `employees` AS empl JOIN `t_employee_role` AS emprole ON empl.EmployeeID = emprole.employee_id ;", nativeQuery = true)
    List<CEmployees> getListAllEmployee();

}
