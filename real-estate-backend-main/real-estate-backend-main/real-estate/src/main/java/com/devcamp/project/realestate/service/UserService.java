package com.devcamp.project.realestate.service;

import com.devcamp.project.realestate.model.CEmployees;
import com.devcamp.project.realestate.security.UserPrincipal;

public interface UserService {
    CEmployees createCEmployees(CEmployees employees);

    UserPrincipal findByUsername(String username);
}
