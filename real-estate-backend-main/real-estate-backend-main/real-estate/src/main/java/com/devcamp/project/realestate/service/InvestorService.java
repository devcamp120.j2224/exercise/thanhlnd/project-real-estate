package com.devcamp.project.realestate.service;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.IInvestorRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvestorService {
    @Autowired
    IInvestorRepository pInvestorRepository;

    public List<CInvestor> getInvestorList() {
        List<CInvestor> investorList = new ArrayList<CInvestor>();
        pInvestorRepository.findAll().forEach(investorList::add);
        return investorList;
    }

    public Object createInvestorObj(CInvestor cInvestor) {

        CInvestor newInvestor = new CInvestor();
        newInvestor.setName(cInvestor.getName());
        newInvestor.setDescription(cInvestor.getDescription());
        newInvestor.setProjects(cInvestor.getProjects());
        newInvestor.setAddress(cInvestor.getAddress());
        newInvestor.setPhone(cInvestor.getPhone());
        newInvestor.setPhone2(cInvestor.getPhone2());
        newInvestor.setFax(cInvestor.getFax());
        newInvestor.setEmail(cInvestor.getEmail());
        newInvestor.setWebsite(cInvestor.getWebsite());
        newInvestor.setNote(cInvestor.getNote());

        CInvestor savedInvestor = pInvestorRepository.save(newInvestor);
        return savedInvestor;
    }

    public Object updateInvestorObj(CInvestor cInvestor, Optional<CInvestor> cInvestorData) {

        CInvestor newInvestor = cInvestorData.get();
        newInvestor.setName(cInvestor.getName());
        newInvestor.setDescription(cInvestor.getDescription());
        newInvestor.setProjects(cInvestor.getProjects());
        newInvestor.setAddress(cInvestor.getAddress());
        newInvestor.setPhone(cInvestor.getPhone());
        newInvestor.setPhone2(cInvestor.getPhone2());
        newInvestor.setFax(cInvestor.getFax());
        newInvestor.setEmail(cInvestor.getEmail());
        newInvestor.setWebsite(cInvestor.getWebsite());
        newInvestor.setNote(cInvestor.getNote());

        CInvestor savedInvestor = pInvestorRepository.save(newInvestor);
        return savedInvestor;
    }
}
