package com.devcamp.project.realestate.controller;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.*;
import com.devcamp.project.realestate.service.UtilityService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class UtilityController {
    @Autowired
    IUtilityRepository pUtilityRepository;

    @Autowired
    UtilityService pUtilityService;

    @GetMapping("/utilities")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getUtilityList() {
        if (!pUtilityService.getUtilityList().isEmpty()) {
            return new ResponseEntity<>(pUtilityService.getUtilityList(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/utilities/{utilityId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getUtilityById(@PathVariable Integer utilityId) {
        if (pUtilityRepository.findById(utilityId).isPresent()) {
            return new ResponseEntity<>(pUtilityRepository.findById(utilityId).get(),
                    HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/utilities")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> createUtility(@RequestBody CUtility cUtility) {
        try {
            return new ResponseEntity<>(pUtilityService.createUtilityObj(cUtility), HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Utility: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/utilities/{utilityId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> updateUtility(@PathVariable("utilityId") Integer utilityId,
            @RequestBody CUtility cUtility) {
        try {
            Optional<CUtility> utilityData = pUtilityRepository.findById(utilityId);
            if (utilityData.isPresent()) {
                return new ResponseEntity<>(pUtilityService.updateUtilityObj(cUtility, utilityData), HttpStatus.OK);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Utility: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/utilities/{utilityId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> deleteUtilityById(@PathVariable("utilityId") int utilityId) {
        try {
            pUtilityRepository.deleteById(utilityId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
