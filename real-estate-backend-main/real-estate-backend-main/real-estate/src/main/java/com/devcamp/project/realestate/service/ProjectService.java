package com.devcamp.project.realestate.service;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectService {
    @Autowired
    IProjectRepository pProjectRepository;

    @Autowired
    IProvinceRepository pProvinceRepository;

    @Autowired
    IDistrictRepository pIDistrictRepository;

    @Autowired
    IWardRepository pIWardRepository;

    @Autowired
    IStreetRepository pIStreetRepository;

    @Autowired
    IInvestorRepository pIInvestorRepository;

    @Autowired
    IConstructionContractorRepository pIConstructionContractorRepository;

    public List<CProject> getProjectList() {
        List<CProject> projectList = new ArrayList<CProject>();
        pProjectRepository.findAll().forEach(projectList::add);
        return projectList;
    }

    public Object createProjectObj(CProject cProject, Integer provinceId, Integer districtId, Integer investorId, Integer constructionContractorId) {

        Optional<CProvince> provinceData = pProvinceRepository.findById(provinceId);
        Optional<CDistrict> districtData = pIDistrictRepository.findById(districtId);
        Optional<CInvestor> investorData = pIInvestorRepository.findById(investorId);
        Optional<CConstructionContractor> constructionContractorData = pIConstructionContractorRepository
                .findById(constructionContractorId);

        CProject newProject = new CProject();
        newProject.setName(cProject.getName());
        newProject.setAddress(cProject.getAddress());
        newProject.setSlogan(cProject.getSlogan());
        newProject.setAcreage(cProject.getAcreage());
        newProject.setConstructArea(cProject.getConstructArea());
        newProject.setNumBlock(cProject.getNumBlock());
        newProject.setNumFloors(cProject.getNumFloors());
        newProject.setNumApartment(cProject.getNumApartment());
        newProject.setApartmenttArea(cProject.getApartmenttArea());
        newProject.setUtilities(cProject.getUtilities());
        newProject.setRegionLink(cProject.getRegionLink());
        newProject.setPhoto(cProject.getPhoto());
        newProject.set_lat(cProject.get_lat());
        newProject.set_lng(cProject.get_lng());
        newProject.setDesignUnit(cProject.getDesignUnit());

        if (provinceData.isPresent()) {
            CProvince _province = provinceData.get();
            newProject.set_province(_province);
        } else {
            newProject.set_province(null);
        }

        if (districtData.isPresent()) {
            CDistrict _district = districtData.get();
            newProject.set_district(_district);
        } else {
            newProject.set_district(null);
        }

        if (investorData.isPresent()) {
            CInvestor _investor = investorData.get();
            newProject.setInvestor(_investor);
        } else {
            newProject.setInvestor(null);
        }

        if (constructionContractorData.isPresent()) {
            CConstructionContractor _constructionContractor = constructionContractorData.get();
            newProject.setConstructionContractor(_constructionContractor);
        } else {
            newProject.setConstructionContractor(null);
        }

        CProject savedProject = pProjectRepository.save(newProject);
        return savedProject;
    }

    public Object updateProjectObj(CProject cProject, Optional<CProject> projectData, Integer provinceId,
            Integer districtId, Integer investorId, Integer constructionContractorId) {

        Optional<CProvince> provinceData = pProvinceRepository.findById(provinceId);
        Optional<CDistrict> districtData = pIDistrictRepository.findById(districtId);
        Optional<CInvestor> investorData = pIInvestorRepository.findById(investorId);
        Optional<CConstructionContractor> constructionContractorData = pIConstructionContractorRepository
                .findById(constructionContractorId);

        CProject newProject = projectData.get();
        newProject.setName(cProject.getName());
        newProject.setAddress(cProject.getAddress());
        newProject.setSlogan(cProject.getSlogan());
        newProject.setAcreage(cProject.getAcreage());
        newProject.setConstructArea(cProject.getConstructArea());
        newProject.setNumBlock(cProject.getNumBlock());
        newProject.setNumFloors(cProject.getNumFloors());
        newProject.setNumApartment(cProject.getNumApartment());
        newProject.setApartmenttArea(cProject.getApartmenttArea());
        newProject.setUtilities(cProject.getUtilities());
        newProject.setRegionLink(cProject.getRegionLink());
        newProject.setPhoto(cProject.getPhoto());
        newProject.set_lat(cProject.get_lat());
        newProject.set_lng(cProject.get_lng());
        newProject.setDesignUnit(cProject.getDesignUnit());

        if (provinceData.isPresent()) {
            CProvince _province = provinceData.get();
            newProject.set_province(_province);
        } else {
            newProject.set_province(null);
        }

        if (districtData.isPresent()) {
            CDistrict _district = districtData.get();
            newProject.set_district(_district);
        } else {
            newProject.set_district(null);
        }

        if (investorData.isPresent()) {
            CInvestor _investor = investorData.get();
            newProject.setInvestor(_investor);
        } else {
            newProject.setInvestor(null);
        }

        if (constructionContractorData.isPresent()) {
            CConstructionContractor _constructionContractor = constructionContractorData.get();
            newProject.setConstructionContractor(_constructionContractor);
        } else {
            newProject.setConstructionContractor(null);
        }

        CProject savedProject = pProjectRepository.save(newProject);
        return savedProject;
    }
}
