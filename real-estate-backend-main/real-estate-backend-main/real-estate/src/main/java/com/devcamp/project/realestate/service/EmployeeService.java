package com.devcamp.project.realestate.service;

import java.util.*;

import com.devcamp.project.realestate.entity.Role;
import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.IEmployeeRepository;
import com.devcamp.project.realestate.repository.RoleRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {
    @Autowired
    RoleRepository pRoleRepository;
    
    @Autowired
    IEmployeeRepository pEmployeeRepository;

    public Object createEmployeeObj(CEmployees cEmployees) {

        CEmployees newEmployee = new CEmployees();
        newEmployee.setLastName(cEmployees.getLastName());
        newEmployee.setFirstName(cEmployees.getFirstName());
        newEmployee.setTitle(cEmployees.getTitle());
        newEmployee.setTitleOfCourtesy(cEmployees.getTitleOfCourtesy());
        newEmployee.setBirthDate(cEmployees.getBirthDate());
        newEmployee.setHireDate(cEmployees.getHireDate());
        newEmployee.setAddress(cEmployees.getAddress());
        newEmployee.setCity(cEmployees.getCity());
        newEmployee.setRegion(cEmployees.getRegion());
        newEmployee.setPostalCode(cEmployees.getPostalCode());
        newEmployee.setCountry(cEmployees.getCountry());
        newEmployee.setHomePhone(cEmployees.getHomePhone());
        newEmployee.setExtension(cEmployees.getExtension());
        newEmployee.setReportsTo(cEmployees.getReportsTo());
        newEmployee.setUsername(cEmployees.getUsername());
        newEmployee.setPassword(new BCryptPasswordEncoder().encode(cEmployees.getPassword()));
        newEmployee.setEmail(cEmployees.getEmail());
        newEmployee.setActivated(cEmployees.getActivated());
        newEmployee.setProfile(cEmployees.getProfile());

        Optional<Role> roleData = pRoleRepository.findById(4);
        Role _role = roleData.get();
        Set<Role> set = new HashSet<Role>();
        set.add(_role);
        newEmployee.setRoles(set);

        CEmployees savedEmployee = pEmployeeRepository.save(newEmployee);
        return savedEmployee;
    }

    public Object updateEmployeeObj(CEmployees cEmployees, Optional<CEmployees> employeeData, Optional<Role> roleData) {

        CEmployees newEmployee = employeeData.get();
        newEmployee.setLastName(cEmployees.getLastName());
        newEmployee.setFirstName(cEmployees.getFirstName());
        newEmployee.setTitle(cEmployees.getTitle());
        newEmployee.setTitleOfCourtesy(cEmployees.getTitleOfCourtesy());
        newEmployee.setBirthDate(cEmployees.getBirthDate());
        newEmployee.setHireDate(cEmployees.getHireDate());
        newEmployee.setAddress(cEmployees.getAddress());
        newEmployee.setCity(cEmployees.getCity());
        newEmployee.setRegion(cEmployees.getRegion());
        newEmployee.setPostalCode(cEmployees.getPostalCode());
        newEmployee.setCountry(cEmployees.getCountry());
        newEmployee.setHomePhone(cEmployees.getHomePhone());
        newEmployee.setExtension(cEmployees.getExtension());
        newEmployee.setReportsTo(cEmployees.getReportsTo());
        newEmployee.setUsername(cEmployees.getUsername());
        newEmployee.setPassword(new BCryptPasswordEncoder().encode(cEmployees.getPassword()));
        newEmployee.setEmail(cEmployees.getEmail());
        newEmployee.setActivated(cEmployees.getActivated());
        newEmployee.setProfile(cEmployees.getProfile());

        Role _role = roleData.get();
        Set<Role> set = new HashSet<Role>();
        set.add(_role);
        newEmployee.setRoles(set);

        CEmployees savedEmployee = pEmployeeRepository.save(newEmployee);
        return savedEmployee;
    }
}
