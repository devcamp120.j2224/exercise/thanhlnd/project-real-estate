package com.devcamp.project.realestate.service;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.ICustomerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
    @Autowired
    ICustomerRepository pCustomerRepository;

    public List<CCustomer> getCustomerList() {
        List<CCustomer> customerList = new ArrayList<CCustomer>();
        pCustomerRepository.findAll().forEach(customerList::add);
        return customerList;
    }

    public Object createCustomerObj(CCustomer cCustomer) {

        CCustomer newCustomer = new CCustomer();
        newCustomer.setContactName(cCustomer.getContactName());
        newCustomer.setContactTitle(cCustomer.getContactTitle());
        newCustomer.setAddress(cCustomer.getAddress());
        newCustomer.setMobile(cCustomer.getMobile());
        newCustomer.setEmail(cCustomer.getEmail());
        newCustomer.setNote(cCustomer.getNote());
        newCustomer.setCreateBy(cCustomer.getCreateBy());
        newCustomer.setUpdateBy(cCustomer.getUpdateBy());
        newCustomer.setCreateDate(new Date());

        CCustomer savedCustomer = pCustomerRepository.save(newCustomer);
        return savedCustomer;
    }

    public Object updateCustomerObj(CCustomer cCustomer, Optional<CCustomer> customerData) {

        CCustomer newCustomer = customerData.get();
        newCustomer.setContactName(cCustomer.getContactName());
        newCustomer.setContactTitle(cCustomer.getContactTitle());
        newCustomer.setAddress(cCustomer.getAddress());
        newCustomer.setMobile(cCustomer.getMobile());
        newCustomer.setEmail(cCustomer.getEmail());
        newCustomer.setNote(cCustomer.getNote());
        newCustomer.setCreateBy(cCustomer.getCreateBy());
        newCustomer.setUpdateBy(cCustomer.getUpdateBy());
        newCustomer.setUpdateDate(new Date());

        CCustomer savedCustomer = pCustomerRepository.save(newCustomer);
        return savedCustomer;
    }
}
