package com.devcamp.project.realestate.service;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.IAddressMapRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressMapService {
    @Autowired
    IAddressMapRepository pAddressMapRepository;

    public List<CAddressMap> getAddressMapList() {
        List<CAddressMap> addressMapList = new ArrayList<CAddressMap>();
        pAddressMapRepository.findAll().forEach(addressMapList::add);
        return addressMapList;
    }

    public Object createAddressMapObj(CAddressMap cAddressMap) {

        CAddressMap newAddressMap = new CAddressMap();
        newAddressMap.setAddress(cAddressMap.getAddress());
        newAddressMap.setLat(cAddressMap.getLat());
        newAddressMap.setLng(cAddressMap.getLng());

        CAddressMap savedAddressMap = pAddressMapRepository.save(newAddressMap);
        return savedAddressMap;
    }

    public Object updateAddressMapObj(CAddressMap cAddressMap, Optional<CAddressMap> addressMapData) {

        CAddressMap newAddressMap = addressMapData.get();
        newAddressMap.setAddress(cAddressMap.getAddress());
        newAddressMap.setLat(cAddressMap.getLat());
        newAddressMap.setLng(cAddressMap.getLng());

        CAddressMap savedAddressMap = pAddressMapRepository.save(newAddressMap);
        return savedAddressMap;
    }

}
